package com.db.application;

public class ApplicationImpl implements IApplication {
    private double balance = 0;
    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public double deposit(String accountId, double amount) {
        return balance + amount;
    }

    @Override
    public double withdraw(String accountId, double amount) {
        return balance - amount;
    }
}
