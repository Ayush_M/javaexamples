package com.db.application;

public class ApplicationLauncher {
    public static void main(String[] args) {
        System.out.println(new ApplicationImpl() instanceof IApplication);

        IApplication application = new ApplicationImpl();
        System.out.println(application.deposit("0001",1000));
        System.out.println(application.withdraw("0002",500));
    }
}
