package com.db.application;

public interface IApplication {

    /**
     * Returns the version of your application
     *
     * @return
     */
    String getVersion();

    /**
     * @param accountId
     * @param amount
     * @return
     */
    double deposit(String accountId, double amount);


    /**
     * @param accountId
     * @param amount
     * @return
     */
    double withdraw(String accountId, double amount);
}
